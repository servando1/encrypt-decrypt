'use strict';
const crypto = require('crypto');
const nodeRSA = require('node-rsa');
const ORIGINAL = "Lorem ipsum dolor sit amet"

function encrypt(val, key, iv) {
    let cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    let encrypted = cipher.update(val, 'utf8', 'base64');
    encrypted += cipher.final('base64');
    return encrypted;
}

function decrypt(encrypted, key, iv) {
    let decipher = crypto.createDecipheriv('aes-256-cbc', key, iv);
    let decrypted = decipher.update(encrypted, 'base64', 'utf8');
    return (decrypted + decipher.final('utf8'));
}

function testAES256() {
    const ENC_KEY = crypto.randomBytes(32);
    const IV = crypto.randomBytes(16);
    const cipher = encrypt(ORIGINAL, ENC_KEY, IV);
    const decipher = decrypt(cipher, ENC_KEY, IV);
    console.log('Line 26 ORIGINAL: ', ORIGINAL);
    console.log('Line 27 cipher: ', cipher);
    console.log('Line 28 decipher: ', decipher);
}

function testRSA() {
    const keys = new nodeRSA({b: 2048});
    const cipher = keys.encrypt(ORIGINAL, 'base64');
    const decipher = keys.decrypt(cipher, 'utf8');
    console.log('Line 26 ORIGINAL: ', ORIGINAL);
    console.log('Line 27 cipher: ', cipher);
    console.log('Line 28 decipher: ', decipher);


}

function init() {
    console.log('Line 32 - TEST AES 256');
    console.log("˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜***************************˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜");
    testAES256();
    console.log("˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜***************************˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜");
    console.log('Line 45 - TEST RSA');
    console.log("˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜***************************˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜");
    testRSA();
    console.log("˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜***************************˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜˜");
}

init();